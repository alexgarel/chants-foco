Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2015-08-29T19:40:30+02:00

====== Verbum Panis ======

CD de chants de messe réalisé par [[http://www.azionimusicali.com|Sandro Crippa]] en collaboration avec [[http://www.gaborit-chanteur.com|André Gaborit]] pour l'édition en Français.
Pour acheter le CD : http://www.gaborit-chanteur.com/Contact
