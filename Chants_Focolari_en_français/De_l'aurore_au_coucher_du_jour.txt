Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2015-08-29T19:55:22+02:00

====== De l'aurore au coucher du jour ======

@m_accueil 

Auteur : Mite Balduzzi @a_mite_balduzzi 
Album : [[Discographie:Verbum Panis|Verbum Panis]]
Adaptation : André Gaborit

===== Partitions =====
Carnet Chant 2010 : [[./De l'aurore au coucher du jour.pdf]]
Partition présente sur le CD : [[./De l'aurore au coucher du jour - Verbum Panis - partition.pdf]]

===== Audio =====
[[./01 - De l'aurore au coucher du jour - Verbum Panis.ogg]]
