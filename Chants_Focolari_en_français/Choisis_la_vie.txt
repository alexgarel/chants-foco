Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2015-08-30T00:42:37+02:00

====== Choisis la vie ======

Auteur : Thérèse Themlin @a_thérèse_themlin
Album : [[Discographie:Prends ta plume|Prends ta plume]] @d_prend_ta_plume 

@t_oser_aimer 

===== Partitions =====
[[./Thérèse Themelin - choisis la vie.pdf]] et [[./Thérèse Themelin - choisis la vie.docx]]

===== Audio =====
[[./Disc 1 - 14_-_Choisis_la_vie.ogg]]
