Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-09-26T22:47:16+02:00

====== Les villes ======

Auteur: Chiara Grillo @a_chiara_grillo 
Titre italien: Le città (album: RunSongs - Run4Unity 2008)
Traduction: JPMU France (Unison) @a_unison 

Texte avec accords:

===== Audio =====
Enregistré lors du [[https://soundcloud.com/user-102303108|week-end unison]] : [[./Les villes (le città) mp3.mp3]]
Enregistrement Alex sur une tonalité différente : [[./les-villes-le-citta-maquette-alex.mp3]]

Original italien (Album RunSongs) : [[./04 Le Città.wma]]
Base musicale : [[./12 Le città (instrumental).wma]]
