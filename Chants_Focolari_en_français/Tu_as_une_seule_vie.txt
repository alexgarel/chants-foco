Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2015-08-22T22:10:14+02:00

====== Tu as une seule vie ======

Auteur : Ragazzi per l'unità @a_rpu 
Album : The Golden rule (Super Congrès 2003) mais également life love light (Béatification Chiara Luce)
Titre italien : hai una vita sola
Adaptation : Emmanuele I.

@t_oser_aimer 

===== Partitions =====
Carnet 2010: [[./Tu as une seule vie.pdf]]

===== Audio =====
version italienne originale : [[./1_-_Hai_una_vita_sola.ogg]]
version italienne life love light : [[./4_-_Una_vita_sola.ogg]]
