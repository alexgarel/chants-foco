Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-09-26T22:56:47+02:00

====== Au creux de mes bras ======

Cette chansons est la suite de l'histoire racontée par [[Chants focolari en d'autres langues:Stelle e lacrime|Stelle e lacrime,]] elle raconte l'expérience de Chiara Lubich à Trente, suite à un intense bombardement.

Auteur: Ragazzi per l'unità @a_rpu 
Album : The Golden Rule (Super Congrès 2003)
Titre original: Tra le mie braccia
Traduction: JPMU France (Unison) @a_unison 

@t_souffrance 

===== Audio =====
Enregistré lors du [[https://soundcloud.com/user-102303108|week-end unison]] : [[./au-creux-de-mes-bras.mp3]]

Original italien: [[./4_-_Tra_le_mie_braccia.ogg]]
